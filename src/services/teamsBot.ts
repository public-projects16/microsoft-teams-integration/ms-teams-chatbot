// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import dotenv from "dotenv";
import {Cards} from "../dialogs/Cards";

const { MyBot } = require('./eventBotService');

dotenv.config();

export class TeamsBot extends MyBot {
    /**
     *
     * @param {ConversationState} conversationState
     * @param {UserState} userState
     * @param {Dialog} dialog
     */


    constructor(conversationState: any, userState: any, dialog: any) {        
        super(conversationState, userState, dialog);

        this.onMembersAdded(async (context: any, next:any) => {
            const membersAdded = context.activity.membersAdded;
            for (let cnt = 0; cnt < membersAdded.length; cnt++) {
                if (membersAdded[cnt].id !== context.activity.recipient.id) {
                    await Cards.sendIntroCard(context);
                }
            }
            await next();
        });


        this.onInvoke(async (context: any, next: any) => {
            console.log('Running dialog with Invoke Activity.');
            await this.dialog.run(context, this.dialogState);

            await next();
        });
    }
}