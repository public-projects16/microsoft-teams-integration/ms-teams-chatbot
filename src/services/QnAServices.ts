import * as dotenv from "dotenv";
const { QnAMaker } = require('botbuilder-ai');

dotenv.config();

class QnAService {
    private qnaMaker: any;
    constructor() {
        this.configurate();
    }
    configurate() {
        try {
            this.qnaMaker = new QnAMaker({
                knowledgeBaseId: process.env.QnAKnowledgebaseId,
                endpointKey: process.env.QnAEndpointKey,
                host: process.env.QnAEndpointHostName
            });
        } catch (err) {
            console.warn(`QnAMaker Exception: ${ err } Check your QnAMaker configuration in .env`);
        }
    }
    async getAnswer(question: string) {
        const qnaResults = await this.qnaMaker.generateAnswer(question);
        if (qnaResults[0]) {
            return qnaResults[0].answer;
        } else {
          let error_message =  "No answer"
          //No tengo la respuesta a su pregunta, lo contactaré de inmediato con alguien de servicios.";
          //error_message += " O bien puedes contactar a través del correo soporte@netlogistik.com o llamando a 55 5282 1321 Ext 5280";
          return error_message;
        }
    }
}

const qna: QnAService = new QnAService();
export default qna;