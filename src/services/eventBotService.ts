import { TeamsActivityHandler } from '../teamsActivityHandler';

export class MyBot extends TeamsActivityHandler {
    conversationState: any;
    userState: any;
    dialog: any;
    dialogState: any;
    
    constructor(conversationState: any, userState: any, dialog: any) {
        super();
        
        this.conversationState = conversationState;
        this.userState = userState;
        this.dialog = dialog;
        this.dialogState = this.conversationState.createProperty('DialogState');

        this.onMessage(async (context: any, next: any) => {
            await this.dialog.run(context, this.dialogState);
            await next();
        });

        this.onDialog(async (context: any, next: any) => {
            // Save any state changes. The load happened during the execution of the Dialog.
            await this.conversationState.saveChanges(context, false);
            await this.userState.saveChanges(context, false);

            await next();
        });

    }
}