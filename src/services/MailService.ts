import * as dotenv from "dotenv";
import * as  mailer from "nodemailer";
import smtpTransport from "nodemailer-smtp-transport";

dotenv.config();

export class Mail {

    private transporter: any;
    private mailOptions: object;
    constructor() {
        this.createTransporter();
    }

    private async createTransporter() {
        this.transporter = mailer.createTransport(smtpTransport({
            service: "gmail",
            auth: {
                user: process.env.ABI_MAIL,
                pass: process.env.EMAIL_PASS
            }
        }));
    }

    private async setMailOptions(header: string, message: any) {
        this.mailOptions = {
            from: "Abi bot <" + process.env.ABI_MAIL + ">",
            to: process.env.SUPPORT_MAIL,
            subject: header,
            html: message
        };
    }

    public async sendEmail(header: string, message: any) {
        await this.setMailOptions( header, message);
        let result = true;
        await this.transporter.sendMail(this.mailOptions, (error: Error, info: any) => {
            if (error) result =  false;
            else result = true;
        });
    return result;
    }
}

const my_mailer: Mail = new Mail();
export default my_mailer;