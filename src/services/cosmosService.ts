import * as cosmos_storage from "azure-storage";
import * as dotenv from "dotenv";
import query_processor from "../utils/QueryProcess";
import { ICosmosInformation } from "../interfaces/ICosmosInformation";
import jwt from "../utils/JWT";
import mail_template from "../templates/mailTemplate";
import my_mailer from "../services/MailService";

dotenv.config();

class CosmosService {
    private table: cosmos_storage.TableService;
    private entityGenerator: any;
    private table_name: string;
    private partition_key_counter: number;
    private cosmos_query: cosmos_storage.TableQuery;
    private response: any;

    constructor() {
        this.table = cosmos_storage.createTableService(process.env.cosmos_connection);
        this.entityGenerator = cosmos_storage.TableUtilities.entityGenerator;
        this.table_name = process.env.cosmos_table_name;
        this.cosmos_query = new cosmos_storage.TableQuery();
        this.createTable(this.table_name);
        this.partition_key_counter = 1;
    }

    async saveOnCosmos(context: any, question: string, answer:  string) {
        const entity = {
            PartitionKey: this.entityGenerator.String((this.partition_key_counter).toString()),
            RowKey: this.entityGenerator.String(context.context.activity.from.name),
            Question: this.entityGenerator.String(question),
            Answer: this.entityGenerator.String(answer) 
        };
        this.newEntity(entity);
        this.partition_key_counter += 1;
    }

    newEntity( entity: object) {
        this.table.insertOrMergeEntity(this.table_name, entity, (err, res, result) => {
            if (err) console.log("Chale"); });
    } 

    deleteEntities(entities: Array<object>) {
        for (const entity of entities) {
            this.table.deleteEntity(this.table_name, entity, (err, res) => {
                if (err) console.log("No se pudo eliminar la entidad");
            });
        }
    }

    createTable(name: string) {
        this.table.createTableIfNotExists( name , (err , res) => {
            if (err) console.log(err); });
    }

    async connectAgain() {
        this.table = cosmos_storage.createTableService(process.env.cosmos_connection);
        this.cosmos_query = new cosmos_storage.TableQuery();
    }

    async getEntitiesForName(name: string) {
        const query: any = this.cosmos_query.select(["PartitionKey", "RowKey", "Answer", "Question"])
        .where('RowKey eq ?', name);
        this.table.queryEntities( this.table_name, query, null, (err, result, response) => { 
            if (!err) {
                this.response = result.entries;
                this.connectAgain();
            }
        });
    }

    async sendMail(name: string, token: string) {
        const query: any = this.cosmos_query.select(["PartitionKey", "RowKey", "Answer", "Question"])
        .where('RowKey eq ?', name);
       this.table.queryEntities( this.table_name, query, null, (err, result, response) => {
            if (!err)  {
                this.connectAgain();
                const cosmos_information: ICosmosInformation = query_processor.processResult( result.entries ); 
                cosmos_information["email"] = jwt.getEmail(token);
                const template = mail_template.getSupportMail(cosmos_information);
                my_mailer.sendEmail("Ayuda soporte. Abi bot", template);
            } else {
                console.log("errror de la función send mail de cosmos");
            }
        });
    }

    get QueryResponse() {
        return this.response;
    }

    set QueryResponse(response: any) {
        this.response = response;
    }
}

const cosmos: CosmosService = new CosmosService();
export default cosmos;