import { TeamsBot } from '../services/teamsBot';
import { MainDialog } from '../dialogs/MainDialog';
import { BotFrameworkAdapter , ConversationState, MemoryStorage, UserState} from 'botbuilder';
import { Request, Response } from "express";
import * as dotenv from "dotenv";

dotenv.config();

export class MainService {
    private memoryStorage: MemoryStorage;
    private conversationState: ConversationState;
    private userState: UserState;
    private dialog: MainDialog;
    private bot: TeamsBot;
    private adapter: BotFrameworkAdapter;

    constructor(){
        this.config();
    }


    private config() {
        this.memoryStorage = new MemoryStorage();
        this.conversationState = new ConversationState(this.memoryStorage);
        this.userState = new UserState(this.memoryStorage);
        this.dialog = new MainDialog();
        this.bot = new TeamsBot(this.conversationState, this.userState, this.dialog);
        this.adapter = new BotFrameworkAdapter({
            appId: process.env.MicrosoftAppId,
            appPassword: process.env.MicrosoftAppPassword
        });
    }

    public botListener(req: Request, res: Response) {
        this.adapter.processActivity(req, res, async (turnContext: any) => {
            await this.bot.run(turnContext);
        });
    }
}

const bot_listener: MainService = new MainService();
export default bot_listener;