// Copyright (c) Microsoft Corporation. All rights reserved.
// Licensed under the MIT License.

import { ActivityHandler, ActivityTypes } from 'botbuilder';

export class TeamsActivityHandler extends ActivityHandler {
    constructor() {
        super();
        this.onUnrecognizedActivityType(async (context: any, next: any) => {
            const runDialogs = async () => {
                await this.handle(context, 'Dialog', async () => {
                    // noop
                });
            };
            if (context.activity.type === ActivityTypes.Invoke )
                await this.handle(context, 'Invoke', runDialogs);
            else
                await next();
        });
    }

    onInvoke(handler: any) {
        return this.on('Invoke', handler);
    }
}