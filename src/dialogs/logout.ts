import { ActivityTypes }  from 'botbuilder';
import { ComponentDialog } from 'botbuilder-dialogs';
import cosmos from "../services/cosmosService";
import { Cards } from "./Cards";
import { spanishDb } from "../utils/spanishDb";
import validator from "../utils/validator";

export class LogoutDialog extends ComponentDialog {
    connectionName: string;
    constructor(id: any, connectionName: any) {
        super(id);
        this.connectionName = connectionName;
    }

    async onBeginDialog(innerDc: any, options: any) {
        const result = await this.interrupt(innerDc);
        if (result) {
            return result;
        }

        return await super.onBeginDialog(innerDc, options);
    }

    async onContinueDialog(innerDc: any) {
        const result = await this.interrupt(innerDc);
        if (result) {
            return result;
        }

        return await super.onContinueDialog(innerDc);
    }

    async interrupt(innerDc: any) {
        if (innerDc.context.activity.type === ActivityTypes.Message) {
            const text = innerDc.context.activity.text.toLowerCase();
            return this.validateCommands(innerDc, text);
        }
    }

    async validateCommands(context: any, text: string) {
        const SpanishDb: spanishDb = new spanishDb();
        SpanishDb.loadBdFromFile();
        if (validator.validateCongruence(text, SpanishDb.analyseTextArray(text))) {
            text = this.normalizedText(text);
            if (text.includes('cierredesesion')) return this.closeSession(context);
            else if (text.includes('terminosdeuso')) return Cards.useTermsCard(context.context);
            else if (text.includes('avisodeprivacidad')) return Cards.privacityCard(context.context);
            else if (text.includes('ayuda') || text.includes('help')) return Cards.helpCard(context.context);
        } else {
            if (text !== 'yes' && text !== 'no')
            return Cards.invalidCommand(context.context);
        }
    }

    normalizedText(text: string) {
        const accents: any = { 'á': 'a', 'é': 'e', 'í':' í', 'ó': 'o', 'ú': 'u', '.': '', '?': '', '¿':'', '!': '', '¡': '', ' ': '', '\n': ''};
        text = text.toLowerCase();
        const split_text: Array<string> = text.split(' ');
        text = split_text.join('')        
        let normalized_text = ""; 
        for (let index = 0; index < text.length; index++) {
            normalized_text += ( (accents[ text[index] ] !== undefined)? accents[ text[index]] : text[index] );
        }
        return normalized_text;
    }

    async closeSession(innerDc: any) {
            const botAdapter = innerDc.context.adapter;
            await botAdapter.signOutUser(innerDc.context, this.connectionName);
            await innerDc.context.sendActivity('Su sesión ha finalizado con éxito.');
            const name = innerDc.context.activity.from.name
            cosmos.getEntitiesForName(name);
            setTimeout(()=> {
                cosmos.deleteEntities(cosmos.QueryResponse);
            }, 6000);
            return await innerDc.cancelAllDialogs();
    }
}
