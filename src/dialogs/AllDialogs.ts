import { ICosmosInformation } from "../interfaces/ICosmosInformation";
import qna from "../services/QnAServices";
import cosmos from "../services/cosmosService";
import query_processor from "../utils/QueryProcess";
import jwt from "../utils/JWT";
import mail_template from "../templates/mailTemplate";
import my_mailer from "../services/MailService";

const translate = require( "@vitalets/google-translate-api");

export class AllDialogs {

    protected CONFIRM_PROMPT: string;
    protected MAIN_DIALOG: string;
    protected MAIN_WATERFALL_DIALOG: string;
    protected OAUTH_PROMPT: string;
    protected TEXT_PROMPT: string;
    protected ABI_PROMT: string;
    protected FINAL_PROMPT: string;
    protected CHOICE_PROMPT: string;

    constructor() {
        this.CONFIRM_PROMPT = 'ConfirmPrompt';
        this.MAIN_DIALOG = 'MainDialog';
        this.MAIN_WATERFALL_DIALOG = 'MainWaterfallDialog';
        this.OAUTH_PROMPT  = 'OAuthPrompt';
        this.TEXT_PROMPT = 'Text prompt';
        this.CHOICE_PROMPT = 'Choice prompt';
        this.ABI_PROMT = 'Abi prompt';
        this.FINAL_PROMPT = 'final abi prompt';
    }

    async promptStep(stepContext: any) {
        return await stepContext.beginDialog(this.OAUTH_PROMPT);
    }

    protected async loginStep(stepContext: any) {
        const tokenResponse = stepContext.result;
        if (tokenResponse) {
            await stepContext.context.sendActivity('Hola es un placer, soy Abibot tu asistente virtual.');
            return stepContext.continueDialog();
        }
        await stepContext.context.sendActivity('Hubo un error al iniciar la sesión.');
        return await stepContext.endDialog();
    }

    protected async displayTokenPhase1(stepContext: any) {
        return await stepContext.beginDialog(this.OAUTH_PROMPT);
    }

    protected async displayTokenPhase2(stepContext: any) {
        const tokenResponse = stepContext.result.token;
        await cosmos.sendMail(stepContext.context.activity.from.name, tokenResponse);
        return await stepContext.prompt(this.CHOICE_PROMPT, "¿ Qué acción desea realizar ?", ["Cierre de sesión" , "Resolver otra duda"]);
    }

    protected async welcome(context: any) {
        return context.beginDialog(this.ABI_PROMT);
    }

    protected async abiFirstStep(step: any) {
        return await step.prompt(this.TEXT_PROMPT, "¿ En que puedo ayudarte ?");
    }

    protected async abiSecondStep(step: any) {
        const qna_response: Array<string> = await qna.getAnswer(step.result);
        if ( qna_response.toString() === "No answer" ) {
            const answer = "No tenemos una respuesta a su pregunta, lo contactaremos de inmediato con alguien de la mesa de soporte.";
            await cosmos.saveOnCosmos(step, step.result, answer )
            await step.context.sendActivity(answer);
            return step.beginDialog(this.FINAL_PROMPT);
        } else {
            try {
                await cosmos.saveOnCosmos(step, step.result, qna_response.toString());
                return translate(qna_response, {to: 'es'}).then( async (res: any) => {
                    await step.context.sendActivity(res.text);
                    return await step.prompt(this.CONFIRM_PROMPT , "¿ Pude resolver tu duda ?");
                }).catch(() => {});
            } catch(err) { console.log('err'); }
        }
    }

    protected async abiThirdStep(step: any) {
        if (step.result) {
            return step.prompt(this.CHOICE_PROMPT, "¿ Qué acción desea realizar ?", ["Cierre de sesión" , "Resolver otra duda"]);
            // return step.prompt(this.CONFIRM_PROMPT , "¿ Tienes alguna otra duda que pueda resolver ?");
        } else {
            await step.context.sendActivity("Lo contactaremos con el personal del área de servicios.");
            return await step.beginDialog(this.FINAL_PROMPT);
        }
    }

    protected async abiFourthStep(step: any) {
        if (step.result) {
            return step.beginDialog(this.ABI_PROMT);
        } else {
            await step.context.sendActivity("Fue un placer ayudarte");
            return step.endDialog();
        }
    }
}