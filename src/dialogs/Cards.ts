import { CardFactory, ActionTypes, MessageFactory} from "botbuilder-core";
import dotenv from "dotenv";

dotenv.config();

export class Cards {

    static async sendIntroCard(context: any) {
        let message = "\n!Hola¡.  Soy Abibot, estoy aquí para resolver tus dudas de transporte en el TMS.";
        message += "<br><br> Para iniciar, basta con presionar el botón <strong>Empezar ahora</strong> y si no tienes una sesión iniciada te ayudaré a loguearte. \n";
        message += "<br><br>Escribe el concepto o reporte del que quieres saber más detalle o bien preguntas del estilo <i>¿Cual es la diferencia entre...?</i>. \n";
        const card = CardFactory.heroCard(
            '!Bienvenido¡',
            message,
            [],
            [
                {
                    type: ActionTypes.OpenUrl,
                    title: 'Aviso de privacidad',
                    value: process.env.privacity_url
                },
                {
                    type: ActionTypes.OpenUrl,
                    title: 'Terminos de uso',
                    value:  process.env.use_terms
                },
                'Empezar ahora']
        );
    
        await context.sendActivity({ attachments: [card] });
    }

    static async helpCard(context: any) {
        let message = "<p>Los comandos que pueden ser utilizados son los siguientes: </p> <br>";
        message += "<li><strong>Ayuda </strong>: Muestra información a cerca de los comandos que pueden usarse.</li>";
        message += "<li><strong>Terminos de uso </strong> : Muestra un botón que redirige a una página donde se muestran nuestros términos de uso </li>";
        message += "<li><strong>Aviso de privacidad </strong> : Muestra un botón que redirige a una página donde se muestra nuestro aviso de privacidad </li>";
        message += "<li><strong>Cierre de sesión  </strong> : Cierra la sesión actual </li>";
        message += '<strong>Al dar click en <i>continuar la conversación</i> reiniciará el flujo del bot.</strong>'
        const card = CardFactory.heroCard(
            'Ayuda.',
            message,
            [],
            [
                'Continuar con la conversación.'
            ]
        );
        const response = MessageFactory.attachment(card);

        return await context.sendActivity(response);
    }

    static async privacityCard(context: any) {
        const card = CardFactory.heroCard(
            "Aviso de privacidad.", "Haz click en el botón para conocer nuestro aviso de privacidad.",
            [],
            [
                {
                    type: ActionTypes.OpenUrl,
                    title: 'Aviso de privacidad',
                    value: process.env.privacity_url
                },
                'Continuar la conversación.'
            ]
        );
        return await context.sendActivity({ attachments: [card] });
    }

    static async useTermsCard(context: any) {
        const card = CardFactory.heroCard(
            "Terminos de uso", "Haz click en el botón para conocer nuestros terminos de uso",
            [],
            [
                {
                    type: ActionTypes.OpenUrl,
                    title: 'Terminos de uso',
                    value: process.env.use_terms
                },
                'Continuar la conversación.'
            ]
        );
        return await context.sendActivity({ attachments: [card] });
    }

    static async invalidCommand(context: any) {
        let message = "<strong>Podrías intentar usando alguno de los siguientes comandos.</strong><br><br>"
        message += "<li><strong>Ayuda </strong>: Muestra información a cerca de los comandos que pueden usarse.</li>";
        message += "<li><strong>Terminos de uso </strong> : Muestra un botón que redirige a una página donde se muestran nuestros términos de uso </li>";
        message += "<li><strong>Aviso de privacidad </strong> : Muestra un botón que redirige a una página donde se muestra nuestro aviso de privacidad </li>";
        message += "<li><strong>Cierre de sesión o logout </strong> : Cierra la sesión actual </li>";
        message += "<br><br><strong><i>Recuerde que todos los comandos y preguntas deben ser en español.</i></strong>"
        const card = CardFactory.heroCard(
            "Lo siento, no pude entender lo que dijiste.", message
            , [] , [ "Continuar la conversación."]
        );
        return await context.sendActivity({ attachments: [card] });
    }
}