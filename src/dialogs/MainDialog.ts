import { DialogSet, DialogTurnStatus } from 'botbuilder-dialogs';
import { LogoutDialog } from './logout';
import prompts from "./AllPrompts";

const MAIN_DIALOG = 'MainDialog';
const MAIN_WATERFALL_DIALOG = 'MainWaterfallDialog';

export class MainDialog extends LogoutDialog {

    constructor() {
        super(MAIN_DIALOG, process.env.connectionName);
        this.addPrompts();
        this.initialDialogId = MAIN_WATERFALL_DIALOG;
    }
    
    /**
     * The run method handles the incoming activity (in the form of a DialogContext) and passes it through the dialog system.
     * If no dialog is active, it will start the default dialog.
     * @param {*} dialogContext
     */
    async run(context: any, accessor: any) {
        const dialogSet = new DialogSet(accessor);
        dialogSet.add(this);
        const dialogContext = await dialogSet.createContext(context);
        const results = await dialogContext.continueDialog();
        if (results.status === DialogTurnStatus.empty) {
            await dialogContext.beginDialog(this.id);
        }
    }

    async addPrompts() {
        this.addDialog(prompts.oauthPrompt());
        this.addDialog(prompts.confirmPrompt());
        this.addDialog(prompts.textPrompt());
        this.addDialog(prompts.choicePrompt());
        this.addDialog(prompts.initialPrompt());
        this.addDialog(prompts.abiBotPrompt());
        this.addDialog(prompts.abiFinalPrompt());
    }
}