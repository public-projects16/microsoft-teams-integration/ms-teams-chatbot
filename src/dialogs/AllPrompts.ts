import { ConfirmPrompt, OAuthPrompt, WaterfallDialog, TextPrompt, ChoicePrompt } from 'botbuilder-dialogs';
import { AllDialogs } from "./AllDialogs";

class AllPrompts extends AllDialogs{

    constructor(){ super(); }

    oauthPrompt (): OAuthPrompt {
        return new OAuthPrompt(this.OAUTH_PROMPT, {
            connectionName: process.env.connectionName,
            text: 'Favor de iniciar sesión',
            title: 'Inicio de sesión',
            timeout: 600000
        });
    }

    confirmPrompt(): ConfirmPrompt {
        return new ConfirmPrompt(this.CONFIRM_PROMPT);
    }

    textPrompt(): TextPrompt {
        return new TextPrompt(this.TEXT_PROMPT);
    }

    choicePrompt(): ChoicePrompt {
        return new ChoicePrompt(this.CHOICE_PROMPT);
    }

    initialPrompt(): WaterfallDialog {
        return new WaterfallDialog(this.MAIN_WATERFALL_DIALOG, [
            this.promptStep.bind(this),
            this.loginStep.bind(this), 
            this.welcome.bind(this)
        ]);
    }

    abiBotPrompt(): WaterfallDialog {
        return new WaterfallDialog(this.ABI_PROMT, [
            this.abiFirstStep.bind(this),
            this.abiSecondStep.bind(this),
            this.abiThirdStep.bind(this),
            this.abiFourthStep.bind(this)
        ]);
    }

    abiFinalPrompt(): WaterfallDialog {
        return new WaterfallDialog(this.FINAL_PROMPT, [
            this.displayTokenPhase1.bind(this),
            this.displayTokenPhase2.bind(this)
        ]);
    }
}

const prompts: AllPrompts = new AllPrompts();
export default prompts;