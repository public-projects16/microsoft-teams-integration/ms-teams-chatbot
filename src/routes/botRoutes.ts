import { Router } from "express";
import  botController  from "../controllers/botController";

class BotRoutes {

    private router: Router;

    constructor() {
        this.router =  Router();
        this.setRoutes();
    }
    setRoutes() {
        this.router.get("/", botController.index);
        this.router.get("/hello", botController.hello);
        this.router.get("/configure", botController.configure);
        this.router.get("/aviso-de-privacidad", botController.first);
        this.router.get("/terminos-de-uso", botController.second);
        this.router.post("/api/messages", botController.message);
    }
    public getRouter(): Router {
        return this.router;
    }
}

const botRoutes: BotRoutes = new BotRoutes();
export default botRoutes.getRouter();