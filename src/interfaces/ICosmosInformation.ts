export interface ICosmosInformation {

    questions: string;
    answers: string;
    name: string;
    email?: string;
    
}