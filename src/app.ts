import express, { Application } from "express";
import cors from "cors";
import path from "path";
import botRoutes from "./routes/botRoutes";
import * as dotenv from "dotenv";
import { Trainer } from "./utils/train";

dotenv.config();

class App {
    private app: Application;

    constructor() {
        this.app = express();
        this.configuration();
        this.routes();
        Trainer.train();
    }

    configuration(): void {
        this.app.set("port", process.env.PORT || 3333);
        this.app.use(cors({origin: true}));
        this.app.use(express.json());
        this.app.use(express.static('./static'));
        this.app.set('view engine', 'pug');
        this.app.set('views',  './views');
        console.log(__dirname);
    }

    routes(): void {
        this.app.use("/" , botRoutes);
    }

    start(): void {
        this.app.listen(this.app.get("port"), () => {
          console.log( "App is running at http://localhost:" + this.app.get("port") );
          console.log("  Press CTRL-C to stop\n ");
        });
      }
}

const BotApp: App = new App();
export default BotApp;