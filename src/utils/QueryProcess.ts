import { ICosmosInformation } from "../interfaces/ICosmosInformation";

class QueryProcess {
    constructor(){}

    processResult(result: any): ICosmosInformation {
        const answers = this.getAnswers(result);
        const questions = this.getQuestions(result);
        const name = this.getName(result);
        const information: ICosmosInformation = {
            questions: questions,
            answers: answers,
            name: name
        } 
        return information;
    }

    private getAnswers(result: any) {
        const answers: Array<string> = [];
        for(const document of result) {
            answers.push(document.Answer._);
        }
        return answers[0];
    }

    private getQuestions(result: any) {
        const questions: Array<string> = [];
        for (const document of result ) {
            questions.push(document.Question._);
        }
        return questions[0];
    }

    public getName(result: any) {
        try {
            return result[0].RowKey._;
        } catch {
            return "Invitado";
        }
    }
}

const query_processor: QueryProcess = new QueryProcess();
export default query_processor;