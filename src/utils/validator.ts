class Validator {
    constructor() {}

    public validateCongruence(text: string,  processing_text: any) {
        let isCorrect: boolean = false; 
        const words: Array<string> = text.split(' ');
        for (const word  of words) {
            if (processing_text[word].length !== 0) {
                isCorrect = true;
                break;
            }
        }
        return isCorrect;
    }
}

const validator: Validator = new Validator();
export default validator;