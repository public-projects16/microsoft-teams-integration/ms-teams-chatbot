import * as webtoken from "jsonwebtoken";

class JWToken {
    constructor(){}

    private decodeToken(token: string) {
        return webtoken.decode(token, {complete: true});
    }

    public getEmail(token: string) {
        try {
            const token_decoded: any = this.decodeToken(token);
            return token_decoded.payload.upn;
        } catch {

        }
    }
}

const jwt: JWToken = new JWToken();
export default jwt;