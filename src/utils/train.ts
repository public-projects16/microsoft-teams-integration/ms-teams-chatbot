//Incluyo el modulo que permite cargar y crear la bd del idioma.
import { dictionaryLoader } from './dictionaryLoader';

export class Trainer {
    constructor() {}

    public static  train() {
        let tool = new dictionaryLoader();
        tool.loadDataFromFile();
        tool.saveTxtToJson();
        tool.makeLanguageJson();
    }
}