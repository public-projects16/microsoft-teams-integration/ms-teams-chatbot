import fs from "fs";
import os from "os";

export class dictionaryLoader {

    private adjectives: Array<string>;
    private adverbs: Array<string>;
    private prepositions: Array<string>;
    private sustantives: Array<string>;
    private verbs: Array<string>;
    private pronouns: Array<string>;
    private people: Array<string>;
    private places: any;


    constructor() {
        this.adjectives = new Array<string>();
        this.adverbs = new Array<string>();
        this.prepositions = new Array<string>();
        this.sustantives = new Array<string>();
        this.verbs = new Array<string>();
        this.pronouns = new Array<string>();
        this.people = new Array<string>();
    }

    public loadDataFromFile() {
        this.adjectives = this.parseFromFile('../dictionary/adjetivos.txt');
		this.adverbs = this.parseFromFile('../dictionary/adverbios.txt');
		this.prepositions = this.parseFromFile('../dictionary/preposiciones.txt');
		this.sustantives = this.parseFromFile('../dictionary/sustantivos.txt');
		this.verbs = this.parseFromFile('../dictionary/verbos.txt');
		this.pronouns = this.parseFromFile('../dictionary/pronombres.txt');
		this.people = this.parseFromFile('../dictionary/nombres_personales.txt');
		this.places = this.parseFromFile('../dictionary/lugares.txt');
    }

    saveTxtToJson() {
		this.arrayToFile('../dictionary/adjetivos.json',this.adjectives);
		this.arrayToFile('../dictionary/adverbios.json',this.adverbs);
		this.arrayToFile('../dictionary/preposiciones.json',this.prepositions);
		this.arrayToFile('../dictionary/sustantivos.json',this.sustantives);
		this.arrayToFile('../dictionary/verbos.json',this.verbs);
		this.arrayToFile('../dictionary/pronombres.json',this.pronouns);
		this.arrayToFile('../dictionary/nombres_personales.json',this.people);
		this.arrayToFile('../dictionary/lugares.json',this.places);		
    }
    
    makeLanguageJson() {
		let lenguage = {
						"adjetivos"		: this.adjectives,
						"adverbios"		: this.adverbs,
						"preposiciones" : this.prepositions,
						"sustantivos"	: this.sustantives,
						"verbos"		: this.verbs,
						"pronombres"	: this.pronouns,
						"personas" 		: this.people,
						"lugares" 		: this.places						
                     };
                     
		this.arrayToFile('../dictionary/spanish.json',lenguage);
    }
    
    cleanStr(cadena: any) {
		cadena = cadena.replace(/á/gi,"a");
		cadena = cadena.replace(/é/gi,"e");
		cadena = cadena.replace(/í/gi,"i");
		cadena = cadena.replace(/ó/gi,"o");
		cadena = cadena.replace(/ú/gi,"u");
		cadena = cadena.replace(/ñ/gi,"n");
		cadena = cadena.replace(/Á/gi,"A");
		cadena = cadena.replace(/É/gi,"E");
		cadena = cadena.replace(/Í/gi,"I");
		cadena = cadena.replace(/Ó/gi,"O");
		cadena = cadena.replace(/Ú/gi,"U");
		cadena = cadena.replace(/Ñ/gi,"N");

		return cadena.toUpperCase();
    }
    
    parseFromFile(file: any) {
		let	str      = fs.readFileSync(file).toString();
		let listaStr = str.split(os.EOL);
		let struct: any   = {};

		listaStr.forEach((str)=>{
			struct[this.cleanStr(str)] = 1 ;
		});
		return struct;
    }
    
    arrayToFile(file: any,data: any)
	{
		return fs.writeFileSync(file,JSON.stringify(data));
	}
}