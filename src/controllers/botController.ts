import { Request, Response, NextFunction } from "express";
import config from "config";
import * as dotenv from "dotenv";
import path from 'path';
import bot_listener from "../services/mainService";

dotenv.config();
class BotController {
    public async index(req: Request, res: Response) {
        res.render("hello");
    }
    public async hello(req: Request, res: Response) {
        res.render("hello");
    }
    public async configure(req: Request, res: Response) {
        res.render("configure");
    }
    public async first(req: Request, res: Response) {
        res.render("aviso-de-privacidad");
    }
    public async second(req: Request, res: Response) {
        res.render("second");
    }
    public message(req: Request, res: Response) {
        bot_listener.botListener(req, res);
    }
    public card(req: Request, res: Response) {
        res.sendFile(path.join(__dirname+'/card.html'))
    }
}

const botController: BotController = new BotController();
export default botController;