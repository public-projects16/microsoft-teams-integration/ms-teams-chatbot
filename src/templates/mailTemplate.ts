import { ICosmosInformation } from "../interfaces/ICosmosInformation";

class MailTemplates {
    constructor(){}

    getSupportMail(information: ICosmosInformation) {
        const first_name = information.name.split(' ')[0];
        let template = "";
        template += "<!DOCTYPE html>";
        template += "<html lang='en'>";
        template += "<head>";
        template += "<meta charset='UTF-8'>";
        template += "<meta name='viewport' content='width=device-width, initial-scale=1.0'>";
        template += "<meta http-equiv='X-UA-Compatible' content='ie=edge'>";
        template += "<title>Support help</title>";
        template += "</head>";
        template += "<style>";
        template += "* {";
        template += "margin: 0;";
        template += "padding: 0; }";
        template += ".header {";
        template += "background-color: #2286c3 ;";
        template += "color: white;";
        template += "height: 5em; }";
        template += ".header h1 {";
        template += "margin-left: 10em;";
        template += "padding-top: 1em; }";
        template += ".main {";
        template += "display: inline-flex;";
        template += "margin-top: 3em; }";
        template += ".message {";
        template += "text-align: justify;";
        template += "width: 20em;";
        template += "font-size: 30px;";
        template += "margin-left: 1em; }";
        template += ".conversation {";
        template += "border-radius: 5px;";
        template += "background-color: #E1E2E1;";
        template += "margin-left: 4em;";
        template += "width: 40em;";
        template += "height: auto;";
        template += "display:block; }";
        template += ".user {";
        template += "padding-top: 1em;";
        template += "padding-left: 4em;";
        template += "border-radius: 5px;";
        template += "float : right;";
        template += "border-style: solid;";
        template += "border-color: #E1E2E1;";
        template += "background-color: #81b9bf; }";
        template += ".bot {";
        template += "padding-top: 1em;";
        template += "padding-right: 4em;";
        template += "border-radius: 5px;";
        template += "float : left;";
        template += "border-color: #E1E2E1;";
        template += "background-color: white;";
        template += "border-style: solid; }";
        template += "</style>";
        template += "<body>";
        template += "<div class='header'>";
        template += "<h1>Solución requerida por parte de "  + information.name + "</h1>";
        template += "</div>";
        template += "<div class='logo'>";
        template += "<img src='https://www.netlogistik.com/es/images/nlk_img/template/logo-netlogistik-compressor.png' alt=''>";
        template += "</div>";
        template += "<div class='main'>";
        template += "<div class='message'>";
        template += "<p>";
        template += "El usuario " + first_name + " se ha puesto en contacto con nosotros a través ";
        template += "de nuestro chatbot Abi bot, pero no pudimos resolver sus dudas.";
        template += "</p>";
        template += "<p>";
        template += "Favor de comunicarse con el por medio de Microsoft teams con el nombre ";
        template += information.name + " o bien por medio de correo electrónico " + information.email + " .";
        template += "</p>";
        template += "</div>";
        template += "<div class='conversation'>";
        // for (let index = 0; index < information.questions.length; index++) {
            template += "<div class='user'>";
            template += first_name + " : <br>";
            template += "<strong>" + information.questions + "</strong>";
            template += "</div><br><br><br>";
            template += "<div class='bot'>";
            template += "Abi bot: <br>";
            template += "<strong> " + information.answers + " </strong>";
            template += "</div><br><br><br>";
        // }
        template += "</div>";
        template += "</div>";
        template += "</body>";
        template += "</html>";
        return template;
    }
}

const mail_template: MailTemplates = new MailTemplates();
export default mail_template;