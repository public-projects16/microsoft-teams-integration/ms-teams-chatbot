#stage 0
FROM node:lts-alpine as build
WORKDIR /app
COPY . /app/
RUN npm install
RUN npm run build --prod

#stage 1
FROM node:lts-alpine
LABEL APP_NAME "Teams chatbot"
LABEL VERSION "0.0.1"
ENV PORT "3333"

ENV QnAKnowledgebaseId "40ff0e8e-2096-4e67-a34e-1ee6e0df2b26"
ENV QnAEndpointKey "972c4dd4-4db8-473e-8d79-216bb1dcdac9"
ENV QnAEndpointHostName "https://qnas-poc-teams.azurewebsites.net/qnamaker"
ENV MicrosoftAppId "e45ac0a2-6b90-47a2-b468-613d5ab9fea1"
ENV MicrosoftAppPassword "VhKAcF>dfWMbh29Y:L&u8fOv@@6]wy8V"
ENV connectionName "abiconnection"
ENV StorageName "strpocteams"
ENV StorageKey "C5cBAZyD91dJMOQzZBYUEUsD3dSi5IgMw8siQat9Nzdg5jn8n5mcFGm/cflUfs4yoB77GA4YRpuoIE48sgOxRw=="
ENV tableName "table-poc-teams"
ENV cosmos_table_name "chatbot"
ENV cosmos_connection "DefaultEndpointsProtocol=https;AccountName=poc-cosmos-db;AccountKey=50v5Z1Yjk0Gud3lQ20IT3hAW6w1xTCkT5obe55o0ubPogBQFHdpMBgpSw484v1caysaUJGr6UtRezT9u3OHRhg==;TableEndpoint=https://poc-cosmos-db.table.cosmos.azure.com:443/;"
ENV ABI_MAIL  "abi.bot.netlogistik@gmail.com"
ENV EMAIL_PASS "netlogistik12345"
ENV SUPPORT_MAIL  "josue.pruebas.1970@gmail.com"
ENV privacity_url  "https://1030-review-develop-3zknud.netlogistik.com/aviso-de-privacidad"
ENV use_terms  "https://1030-review-develop-3zknud.netlogistik.com/terminos-de-uso"

WORKDIR /mst-chatbot
COPY --from=build   /app/dist /mst-chatbot/dist
COPY --from=build   /app/views  /mst-chatbot/views
COPY --from=build   /app/static  /mst-chatbot/static
COPY --from=build   /app/dictionary ../dictionary
COPY --from=build  /app/node_modules /mst-chatbot/node_modules
EXPOSE 3032
RUN ls ../
CMD node dist/index.js